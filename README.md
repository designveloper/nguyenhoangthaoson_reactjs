### 1 What is React?
    * Library for UI
    * Created at FB and Instagram
    * React Native for mobile
    * React using DOM diffing made updating the DOM faster
        + Compares rendered content with the new UI changes
        + Make only the minimal changes necessary
        + Compare JS object
        + Faster than writing to or reading from DOM
### 2 Intro to JSX & Babel
    * JSX: JS as XML, a tag base syntax
    * Babel: help compile JSX syntax into JS syntax that browser support
    * Webpack: module bundle to create static files & help automate process
    * Loading json with webpack:
        - same import syntax
        - config in webpack.config.js
### 3 React Component      
    Syntax:
    * Creating component with createClass() method
    * Creating class with ES6 syntax
    * Stateless functional component: function take property information & return JSX element

### 4 Props and State
    * Default value can be used when another value is not provided
        - 3 way:
            + createClass: add new method getDefaultProps
            + ES6 syntax
            + Stateless syntax:
                + same with ES6
                + provide default value
    * PropType: 
        - Supplying a property type for all different properties.                 
        - Features, but also work as document
    * 3 way to use PropType : 
        - createClass
        - ES6
        - Stateless
    * PropType can be customize       
    * State: represent the possible conditions of application  
        - Editing/saved
        - Logged in/ logged out
    * State in React:
        - Identify the minimal representation of app state
        - Reduce state to as few component as possible
        - Avoid overwriting state variables     
### 5 Route
    * Navigating to different pages
### 6 Forms & Refs   
    * Refs:
        - Reaching out to individual elements to figure out values
### 7 Components Lifecycle
    * There are three mounting lifecycle methods:
        - componentWillMount: only called in 1st time component rendering
        - render
        - componentDidMount: called right after render is called           